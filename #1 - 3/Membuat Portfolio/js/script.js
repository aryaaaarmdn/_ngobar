// event pada saat link di klik
$('.page-scroll ').on('click', function (e) {

    // ambil value href
    let tujuan = $(this).attr('href');

    // tangkap elemen yang bersangkutan
    let elemenTujuan = $(tujuan);

    // pindahkan scroll
    $('html, body').animate({
        scrollTop: elemenTujuan.offset().top - 50
    }, 1250, 'easeInOutExpo');

    e.preventDefault();

});


// parallax
// about 

$(window).on('load', function () {
    $('.pKiri').addClass('pMuncul');
    $('.pKanan').addClass('pMuncul');
});

$(window).scroll(function () {
    let wScroll = $(this).scrollTop();

    // manipulasi elemen yg didalam jumbotron berdasarkan wScroll
    $('.jumbotron img').css({
        'transform': 'translate(0px, ' + wScroll / 4 + '%)',
        'transition': '0s'
    });

    $('.jumbotron h2').css({
        'transform': 'translate(0px, ' + wScroll / 2 + '%)'
    });

    $('.jumbotron p').css({
        'transform': 'translate(0px, ' + wScroll / 1.2 + '%)'
    });


    // portfolio
    if (wScroll > $('.portfolio').offset().top - 400) {
        $('.portfolio .thumbnail').each(function (i) {
            setTimeout(function () {
                $('.portfolio .thumbnail').eq(i).addClass('muncul');
            }, 300 * (i + 1));
        });

    }
})